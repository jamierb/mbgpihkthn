import os
from django.conf import settings

DATABASES = settings.DATABASES

DEBUG = False
# Parse database configuration from $DATABASE_URL
import dj_database_url
DATABASES['default'] =  dj_database_url.config()

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

# Static asset configuration
import os


# BASE_DIR = os.path.dirname(os.path.abspath(__file__))

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# STATIC_ROOT = 'staticfiles'
# STATIC_URL = '/static/'

# STATICFILES_DIRS = (
#     os.path.join(BASE_DIR, 'static'),
# )

STATIC_URL = '/static/'
# STATIC_URL = os.path.join(BASE_DIR, "static_dev", "our_static")

STATIC_ROOT = os.path.join(BASE_DIR, "static_dev", "our_static")
#This will be the production server for static files

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static_dev", "static_root_dev"),
    # os.path.join(BASE_DIR, "static_env")
    # '/var/www/static/',
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static_env", "media_root")
