# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quoter', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='quote_summary',
            name='BRAND',
            field=models.CharField(default=b'AAMI', max_length=255, blank=True),
        ),
        migrations.AddField(
            model_name='quote_summary',
            name='CTYPE',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AddField(
            model_name='quote_summary',
            name='EXTRA1',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AddField(
            model_name='quote_summary',
            name='EXTRA2',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AddField(
            model_name='quote_summary',
            name='PREM',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='vh_dtl',
            name='BODY',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AddField(
            model_name='vh_dtl',
            name='FUEL',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AddField(
            model_name='vh_dtl',
            name='MAKE',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AddField(
            model_name='vh_dtl',
            name='MODEL',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AddField(
            model_name='vh_dtl',
            name='TRANS',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AddField(
            model_name='vh_dtl',
            name='YEAR',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='front_input',
            name='AGE',
            field=models.IntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='front_input',
            name='VRN',
            field=models.CharField(default=b'Registration Number', max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='quote_summary',
            name='EXTRA3',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='vh_dtl',
            name='TYPE',
            field=models.CharField(max_length=255, blank=True),
        ),
    ]
