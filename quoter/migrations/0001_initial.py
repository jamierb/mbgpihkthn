# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='front_input',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('VRN', models.CharField(default=b'Registration Number', max_length=255)),
                ('AGE', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='quote_Summary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('EXTRA3', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='vh_DTL',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('TYPE', models.CharField(max_length=255)),
            ],
        ),
    ]
