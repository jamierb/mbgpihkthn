from django.db import models

# Create your models here.

class front_input(models.Model):
	VRN = models.CharField(blank=True, max_length=255)
	AGE = models.IntegerField(blank=True)

	def __unicode__(self):
		return self.VRN

class quote_Summary(models.Model):
	BRAND = models.CharField(blank=True, max_length=255, default='AAMI')
	PREM = models.IntegerField(blank=True, null=True)
	CTYPE = models.CharField(blank=True, max_length=255)
	EXTRA1 = models.CharField(blank=True, max_length=255)
	EXTRA2 = models.CharField(blank=True, max_length=255)
	EXTRA3 = models.CharField(blank=True, max_length=255)

	def __unicode__(self):
		return self.BRAND

class vh_DTL(models.Model):
	MAKE = models.CharField(blank=True, max_length=255)
	MODEL = models.CharField(blank=True, max_length=255)
	YEAR = models.CharField(blank=True, max_length=255)
	BODY = models.CharField(blank=True, max_length=255)
	TRANS = models.CharField(blank=True, max_length=255)
	FUEL = models.CharField(blank=True, max_length=255)
	TYPE = models.CharField(blank=True, max_length=255)

	def __unicode__(self): 
		return self.self

