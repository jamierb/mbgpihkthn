from django.contrib import admin
from .models import *

# Register your models here.

class front_input_admin(admin.ModelAdmin):
	class meta:
		model = front_input 
admin.site.register(front_input, front_input_admin)

class quote_Summary_admin(admin.ModelAdmin):
	class meta:
		model = quote_Summary 
admin.site.register(quote_Summary, quote_Summary_admin)

class vh_DTL_admin(admin.ModelAdmin):
	class meta:
		model = vh_DTL 
admin.site.register(vh_DTL, vh_DTL_admin)
